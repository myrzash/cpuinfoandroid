package kz.home.testapp

import kz.home.cpu.CpuModule
import org.koin.core.module.Module

object KoinModules {
    val modules: List<Module> =
        listOf(
            CpuModule.create()
        )
}
