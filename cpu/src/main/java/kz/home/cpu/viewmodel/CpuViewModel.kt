package kz.home.cpu.viewmodel

import androidx.lifecycle.MutableLiveData
import kz.home.common.base.BaseViewModel
import kz.home.common.base.SingleLiveEvent
import kz.home.common.extensions.launchSafe
import kz.home.cpu.api.GetCpuModelNameUseCase
import kz.home.cpu.api.GetCpuTypeUseCase
import kz.home.cpu.api.GetInfoUseCase
import timber.log.Timber

class CpuViewModel(
    private val getInfoUseCase: GetInfoUseCase,
    private val getCpuTypeUseCase: GetCpuTypeUseCase,
    private val getCpuModelNameUseCase: GetCpuModelNameUseCase
) : BaseViewModel() {
    private val _action = SingleLiveEvent<Action>()
    val action: MutableLiveData<Action> = _action

    fun getInfo() {
        launchSafe(
            body = {
                val result = getInfoUseCase(Unit)
                _action.postValue(Action.ShowCpuInfo(result))
            },
            handleError = {
                Timber.e(it, "Error to get cpu type")
            }
        )
    }

    fun load() {
        launchSafe(
            body = {
                val type = getCpuTypeUseCase(Unit)
                _action.postValue(Action.ShowCpuType(type))
                val modelName = getCpuModelNameUseCase(Unit)
                _action.postValue(Action.ShowCpuModelName(modelName))
            },
            handleError = {
                Timber.e(it, "Error to get cpu type")
            }
        )
    }

    sealed class Action {
        data class ShowCpuModelName(val name: String) : Action()
        data class ShowCpuType(val type: String) : Action()
        data class ShowCpuInfo(val text: String) : Action()
    }
}