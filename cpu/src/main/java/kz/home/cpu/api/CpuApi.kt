package kz.home.cpu.api

import android.os.Build
import java.io.File
import java.io.IOException

private const val CPU_FILE_PATH = "/proc/cpuinfo"

class CpuApi {
    suspend fun getInfo(): String {
        val sb = StringBuffer()
        sb.append("").append(Build.CPU_ABI).append("\n")

        val file = File(CPU_FILE_PATH)

        if (file.exists()) {
            try {
                file.bufferedReader().forEachLine {
                    sb.append(it + "\n")
                }

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return sb.toString()
    }
}