package kz.home.cpu.api

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kz.home.common.base.UseCase

private const val MODEL_NAME = "model name"

class GetCpuModelNameUseCase(
    private val cpuApi: CpuApi
) : UseCase<Unit, String>() {
    override val dispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun execute(parameters: Unit): String =
        cpuApi
            .getInfo()
            .split("\n")
            .findLast { it.startsWith(MODEL_NAME) }!!
            .removePrefix(MODEL_NAME)
            .trim()
            .removePrefix(":")
            .trim()
}