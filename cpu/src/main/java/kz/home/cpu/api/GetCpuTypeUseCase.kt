package kz.home.cpu.api

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kz.home.common.base.UseCase


class GetCpuTypeUseCase(
    private val cpuApi: CpuApi
) : UseCase<Unit, String>() {
    override val dispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun execute(parameters: Unit): String = cpuApi
        .getInfo()
        .split("\n")[0]
}