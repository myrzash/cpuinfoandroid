package kz.home.cpu.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_cpu_details.*
import kz.home.common.extensions.observeNotNull
import kz.home.cpu.R
import kz.home.cpu.viewmodel.CpuViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class CpuDetailsFragment : Fragment(R.layout.fragment_cpu_details) {

    private val viewModel: CpuViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.action.observeNotNull(viewLifecycleOwner) {
            when (it) {
                is CpuViewModel.Action.ShowCpuInfo -> infoTextView.text = it.text
            }
        }
        viewModel.getInfo()
    }
}