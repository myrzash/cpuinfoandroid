package kz.home.cpu.ui

import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_cpu.*
import kotlinx.android.synthetic.main.fragment_cpu.view.*
import kz.home.common.extensions.observeNotNull
import kz.home.cpu.R
import kz.home.cpu.viewmodel.CpuViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class CpuFragment : Fragment(R.layout.fragment_cpu) {
    private val viewModel: CpuViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        detailButton.setOnClickListener {
            findNavController().navigate(R.id.actionOpenCpuDetail)
        }

        viewModel.action.observeNotNull(viewLifecycleOwner) {
            when (it) {
                is CpuViewModel.Action.ShowCpuType -> typeTextView.text = it.type
                is CpuViewModel.Action.ShowCpuModelName -> nameTextView.text = it.name
            }
        }
        viewModel.load()
    }
}