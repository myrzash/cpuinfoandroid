package kz.home.cpu

import kz.home.common.base.InjectionModule
import kz.home.cpu.api.CpuApi
import kz.home.cpu.api.GetCpuModelNameUseCase
import kz.home.cpu.api.GetCpuTypeUseCase
import kz.home.cpu.api.GetInfoUseCase
import kz.home.cpu.viewmodel.CpuViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object CpuModule : InjectionModule {
    override fun create() = module {
        single { CpuApi() }
        single { GetInfoUseCase(get()) }
        single { GetCpuTypeUseCase(get()) }
        single { GetCpuModelNameUseCase(get()) }
        viewModel { CpuViewModel(get(), get(), get()) }
    }
}